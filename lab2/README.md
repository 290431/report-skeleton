# Лабораторная работа 2

**Название:** "Разработка драйверов блочных устройств"

**Цель работы:** "Получить знания и навыки разработки драйверов блочных устройств для операционной системы Linux."

## Описание функциональности драйвера

Драйвер создаёт виртуальный жёсткий диск в оперативной памяти ёмкостью 50 MiB. Диск содержит 3 первичных раздела с размерами 10, 25 и 15 MiB.

## Инструкция по сборке
```
make clean
make
make test
...
make exit
```
## Инструкция пользователя

После загрузки модуля появится новые файлы /dev/vramdisk, /dev/vramdisk1, /dev/vramdisk2, /dev/vramdisk3. 


## Примеры использования

```
mkfs.vfat /dev/vramdisk1 - создать файловую систему vfat соответствующему разделу
mkfs.vfat /dev/vramdisk2 
mkfs.vfat /dev/vramdisk3 

mkdir /mnt/vramdisk1 - создать директорию для монтирования диска
mkdir /mnt/vramdisk2
mkdir /mnt/vramdisk3

mount -t vfat /dev/vramdisk1 /mnt/vramdisk1 - смонтировать фс в директорию
mount -t vfat /dev/vramdisk2 /mnt/vramdisk2
mount -t vfat /dev/vramdisk3 /mnt/vramdisk3

```
## Примеры разделов

![Пример](g_razdeli.jpg)
![Пример](t_razdeli.jpg)

## Время копирования из раздела в раждел и из раздела в реальный диск

![Пример](kopirovanie.jpg)
